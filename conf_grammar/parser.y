%token IDENTIFIER STRING
%token ASSIGN OENV OBRACE EBRACE COMMA SEMICOLON

%locations

%{
        #include <stdio.h>
        #include <string.h>
        #include "../includes/llist.h"
        #include "../includes/conf.h"
        #define YYERROR_VERBOSE
        extern int yylex();
        extern int line_num;
        static struct config *parsed_conf;
        int yydebug=1;
        char err_str[256];
        int err;
        void yyerror(const char *s) { printf("ERROR[%d]: %s\n", line_num, s); }
        void remove_quotes(char *str);
%}

%union {
        char *strval;
        struct dlist *listval;
        int token;
}

%type<strval> IDENTIFIER STRING
%type<listval> list

%%
/* Yacc definitions */

expressions:
           |
           expressions expression SEMICOLON
           ;

expression: /* empty */
          |
          IDENTIFIER ASSIGN STRING
          {
          remove_quotes($3);
          if ((err = conf_add_line(parsed_conf, $1, $3)) != 0) {
                if (conf_strerror(err, err_str, sizeof err_str) == NULL)
                        strcpy(err_str, "Unknown error.");

                fprintf(stderr, "Error inserting line at line %d: %s.\n",
                        line_num, err_str);
          }
          }
          |
          IDENTIFIER ASSIGN OBRACE list EBRACE
          {
          if ((err = conf_add_list(parsed_conf, $1, $4)) != 0) {
                if (conf_strerror(err, err_str, sizeof err_str) == NULL)
                        strcpy(err_str, "Unknown error.");

                fprintf(stderr, "Error inserting list at line %d: %s.\n",
                        line_num, err_str);
          }
          }
          |
          IDENTIFIER ASSIGN OENV STRING EBRACE
          {
          remove_quotes($4);
          if ((err = conf_add_env(parsed_conf, $1, $4)) != 0) {
                if (conf_strerror(err, err_str, sizeof err_str) == NULL)
                        strcpy(err_str, "Unknown error.");

                fprintf(stderr, "Error inserting env var at line %d: %s.\n",
                        line_num, err_str);
          }
          }
          ;

list: /* empty */
    {
    $$ = NULL;
    }
    |
    list COMMA STRING
    {
    remove_quotes($3);
    $$ = dlist_add($1, $3, 0);
    }
    | STRING
    {
    remove_quotes($1);
    $$ = dlist_add(NULL, $1, 0);
    }
    ;

%%

void
remove_quotes(char *str)
{
        char *strp = str;
        char *nextp = str;
        int len;

        if (str == NULL)
                return;

        len = strlen(str);

        if (str[len-1] == '"') {
                str[len-1] = '\0';
                len--;
        }

        /*
         * We have to left shift the entire string in this case.
         */
        if (*strp == '"') {
                nextp++;

                while (*nextp != '\0')
                        *strp++ = *nextp++;
                *strp = '\0';
        }
}

int
conf_parse(struct config *conf)
{
        parsed_conf = conf;
        return yyparse();
}
