%option noyywrap yylineno nounput

L           [a-zA-Z_]

%{
#include <stdio.h>
#include "parser.h"
#define TOKEN(t) (yylval.token = t)
#define SAVE_TOKEN yylval.strval = strdup(yytext);
#define MAX_STR_CONST 512

char string_buf[MAX_STR_CONST];
char *string_buf_ptr;
int line_num = 1;
%}

%x comment
%x str

%%

\"      string_buf_ptr = string_buf; BEGIN(str);

<str>\"        { /* saw closing quote - all done */
BEGIN(INITIAL);
*string_buf_ptr = '\0';
/* return string constant token type and
* value to parser
*/
yylval.strval = strdup(string_buf);
return STRING;
}

<str>\n        {
/* error - unterminated string constant */
/* generate error message */
}

<str>\\[0-7]{1,3} {
/* octal escape sequence */
int result;

(void) sscanf( yytext + 1, "%o", &result );

if ( result > 0xff )
 /* error, constant is out-of-bounds */
*string_buf_ptr++ = result;
}

<str>\\[0-9]+ {
/* generate error - bad escape sequence; something
* like '\48' or '\0777777'
*/
}

<str>\\n  *string_buf_ptr++ = '\n';
<str>\\t  *string_buf_ptr++ = '\t';
<str>\\r  *string_buf_ptr++ = '\r';
<str>\\b  *string_buf_ptr++ = '\b';
<str>\\f  *string_buf_ptr++ = '\f';

<str>\\(.|\n) *string_buf_ptr++ = yytext[1];


<str>[^\\\n\"]+        {
char *yptr = yytext;

while ( *yptr ) {
        *string_buf_ptr++ = *yptr++;
}
}

[ \t]+                  /* ignore whitespace */;
#.*                     /* remove comments */;
^[\/][\/].*             /* remove comments */;
"/*"         BEGIN(comment);
<comment>[^*\n]*        /* eat anything that's not a '*' */
<comment>"*"+[^*/\n]*   /* eat up '*'s not followed by '/'s */
<comment>\n             ++line_num;
<comment>"*"+"/"        BEGIN(INITIAL);

[a-zA-Z_][a-zA-Z0-9_]*  SAVE_TOKEN; return IDENTIFIER;
L?\"(\\.|[^\\"])*\"     SAVE_TOKEN; return STRING;
\=                      return TOKEN(ASSIGN);
\$\{                    return TOKEN(OENV);
\{                      return TOKEN(OBRACE);
\}                      return TOKEN(EBRACE);
,                       return TOKEN(COMMA);
;                       return TOKEN(SEMICOLON);
\n                      line_num++;

%%
