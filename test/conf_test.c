#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>

#include <conf.h>

#define UNUSED(x) (void)x

#define NUM_VARS 1000000
#define VAR_VAL_LEN 10
#define TEST_FILE ".test.conf"

static char *env_str = NULL;
static char *var_strings[NUM_VARS];
static char *val_strings[NUM_VARS];

static size_t
generate_random_string(char *str, size_t len)
{
        int i;
        int tlen;
        static char str_table[] =
            "abcdefghijklmnopqrstuvwxyz"
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        tlen = strlen(str_table);

        for (i = 0; i < (len - 1); ++i) {
                str[i] = str_table[random() % tlen];
        }

	str[len] = '\0';

        return len;
}

static int
test_line(struct config *conf_h, char *var, const char *expected)
{
        char *line;

        line = conf_get_line(conf_h, var);
        if (line == NULL) {
                conf_perror("conf_get_line");
                return -1;
        }

        if (strcmp(line, expected) != 0) {
                fprintf(stderr, "Mismatch.  Expected %s but got %s\n",
                        expected, line);
                return -1;
        }

        return 0;
}

int
main(int argc, char **argv, char **envp) {
        int            i;
        char           state[256];
        FILE          *test_write = NULL;
        char          *line = NULL;
        struct config *conf_h;

        UNUSED(argc);
        UNUSED(argv);

        /*
         * Initialize entropy.
         */
        initstate((unsigned int)time(NULL), state, sizeof state);
        setstate(state);

        if ((test_write = fopen(TEST_FILE, "w")) == NULL) {
                fprintf(stderr, "Error opening config file.  %s\n",
                        strerror(errno));
                exit(EXIT_FAILURE);
        }

        for (i = 0; i < NUM_VARS; ++i) {
                var_strings[i] = malloc(VAR_VAL_LEN);
                val_strings[i] = malloc(VAR_VAL_LEN);
                generate_random_string(var_strings[i], VAR_VAL_LEN);
                generate_random_string(val_strings[i], VAR_VAL_LEN);
                fprintf(test_write, "%s = \"%s\";\n", var_strings[i],
                        val_strings[i]);
        }

	fprintf(test_write, "env_test = ${\"PWD\"};\n");

        fclose(test_write);

        conf_h = parse_config(TEST_FILE);
        if (conf_h == NULL) {
                conf_perror("parse_config");
                exit(EXIT_FAILURE);
        }

        for (i = 0; i < NUM_VARS; ++i) {
                if (test_line(conf_h, var_strings[i], val_strings[i]) != 0) {
                        fprintf(stderr, "Mismatch on line %d\n", i);
                        exit(EXIT_FAILURE);
                }

                free(var_strings[i]);
                free(val_strings[i]);
        }

	if ((line = conf_get_line(conf_h, "env_test")) == NULL) {
		fprintf(stderr, "Environment variable test failed.\n");
		fprintf(stderr, "Note that this tests for the PWD variable so "
			"if that's not set in your environment\nThen this "
			"could be a false positive.\n");
	}

        conf_close(conf_h);

        fprintf(stderr, "Test passed!\n");

        return 0;
}

