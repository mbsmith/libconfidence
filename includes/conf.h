/*
 * Copyright (C) 2009 Micheal Smith <xulfer at me.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project author nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file conf.h
 * libconfidence interface.
 *
 * @mainpage This describes the usage of libconfidence.
 * Not everything is currently documented, but things like dlist and what not
 * will be documented soon.  In the meantime refer to llist.h to determine usage
 * of lists of values, and what not.
 */

#ifndef _CONF_H
#define	_CONF_H

#include <stdio.h>

#undef BEGIN_C_DECLS
#undef END_C_DECLS
#ifdef __cplusplus
#define BEGIN_C_DECLS extern "C" {
#define END_C_DECLS }
#else
#define BEGIN_C_DECLS
#define END_C_DECLS
#endif

/**
 * Minimum size an error buffer should be.
 */
#define CONF_ERR_LEN 256

BEGIN_C_DECLS

/**
 * @enum config_errors
 * Error codes returned from the various config functions.
 */
enum config_errors {
	CONF_OK,            /**< Okay.  No errors.                             */
	CONF_HASH,          /**< Hash related error.                           */
	CONF_LINE_PARSE,    /**< Error parsing a config line.                  */
        CONF_PARSE,         /**< Syntax parsing error.                         */
	CONF_SYNTAX,        /**< Syntax error.                                 */
	CONF_NOTFOUND,      /**< A config value couldn't be found.             */
        CONF_INSERT,        /**< Error inserting value into hash, or whatever. */
	CONF_INIT,          /**< Couldn't initialize config parser.            */
	CONF_BADTYPE,       /**< Value type given that is unsupported.         */
	CONF_ERRNO          /**< OS level error.                               */
};

struct config_error {
	int             error;
	char            errstr[CONF_ERR_LEN];
};

/**
 * Configuration handle that contains information regarding a configuration
 * file.
 */
struct config {
        FILE           *conf_handle;    /**< Handle for config file.     */
	char           *filename;	/**< name of the config file.    */
	struct hashtable *config_hash;	/**< hash of config options.     */
};

/**
 * Types of values used by the parser.  (Environment stuff is parsed into a
 * line).
 */
enum value_types {
	CONF_LINE_VALUE,    /**< A string, or environment dereference. */
	CONF_LIST_VALUE     /**< List value                            */
};

/**
 * A value type that contains variable / type data as well as value data.
 */
struct config_value {
	int             type;             /**< Type based on value_types.              */
	char           *varname;          /**< Name of the variable referenced.        */
	union {
		char           *line;     /**< References a line if it's a line value. */
		struct dlist   *values;   /**< List of values if it's a list value.    */
		struct config_value *cv;  /**< Link to another config value.           */
	} value;                          /**< Union of possible value types.          */
};

/**
 * Prints an error message to stderr regarding the current error.  This works
 * similarly to errno/perror.
 *
 * @param prefix A prefix that is prepended to the error message printed.
 */
void conf_perror(const char *prefix);

/**
 * Outputs an error message to a supplied buffer that corresponds to the given
 * error code.
 *
 * @param error[in] Error code taken from one of the supplied functions.
 * @param str[out] Supplied buffer where error string will be output.
 * @param siz[in] Length of supplied buffer.
 *
 * @return Pointer to the error string.
 */
char *conf_strerror(int error, char *str, size_t siz);

/**
 * Should be called first.  This initializes the config structure and parses
 * the configuration file at the given path.
 *
 * @param path Path to the configuration file.
 *
 * @return An allocated, and populated config structure.
 */
struct config *parse_config(const char *path);

/**
 * Close a configuration file / structure.  This should be called when the
 * configuration file / data is no longer needed.  Once this is called no
 * configuration variables can be accessed.
 *
 * @param conf Config structure returned from a call to parse_config().
 */
void conf_close(struct config *conf);

/**
 * Retreives a list of values for a given config variable from the configuration
 * hash.
 *
 * @param conf Config structure returned from a call to parse_config().
 * @param key Variable name to perform the value lookup on.
 *
 * @return List of values in a dlist struct.
 */
struct dlist *conf_get_list(struct config *conf, char *key);

/**
 * Retreives a line for a given config variable from the configuration hash.
 *
 * @param conf Config structure returned from a call to parse_config().
 * @param key Variable name to perform the value lookup on.
 *
 * @return String value for the given key.
 */
char *conf_get_line(struct config *conf, char *key);

/**
 * Adds a line to the configuration hash.  This is generally only called by the
 * parser currently.
 *
 * @param conf Parsed, or at least allocated config structure.
 * @param key Key to add to the configuration hash.
 * @param value Line value to add to the given key.
 *
 * @return CONF_OK on success, or an error code otherwise.
 */
int conf_add_line(struct config *conf, char *key, char *value);

/**
 * Adds a list of values to the configuration hash.  This is generally only
 * called by the parser currently.
 *
 * @param conf Parsed, or at least allocated config structure.
 * @param key Key to add to the configuration hash.
 * @param values List of values to add to the given key.
 *
 * @return CONF_OK on success, or an error code otherwise.
 */
int conf_add_list(struct config *conf, char *key, struct dlist *values);

/**
 * Adds a dereferenced environment value to the configuration hash.  This is
 * generally only called by the parser currently.
 *
 * @param conf Parsed, or at least allocated config structure.
 * @param key Key to add to the configuration hash.
 * @param value Dereferenced environment value.
 *
 * @return CONF_OK on success, or an error code otherwise.
 */
int conf_add_env (struct config *conf, char *key, char *value);

END_C_DECLS
#endif				/* _CONFIG_H */
