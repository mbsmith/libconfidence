/*
 * llist.h
 * Copyright (C) Micheal Smith 2008 <xulfer@me.com>
 *
 * llist.h is free software copyrighted by Micheal Smith.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name ``Micheal Smith'' nor the name of any other
 *    contributor may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * llist.h IS PROVIDED BY Micheal Smith ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL Micheal Smith OR ANY OTHER CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _LLIST_H
#define	_LLIST_H

#undef BEGIN_C_DECLS
#undef END_C_DECLS
#ifdef __cplusplus
#define BEGIN_C_DECLS extern "C" {
#define END_C_DECLS }
#else
#define BEGIN_C_DECLS
#define END_C_DECLS
#endif

#undef PARAMS
#if defined (__STDC__) || defined (_AIX) \
        || (defined (__mips) && defined (_SYSTYPE_SVR4)) \
        || defined(WIN32) || defined(__cplusplus)
#define PARAMS(protos) protos
#else
#define PARAMS(protos) ()
#endif

#include <stdio.h>

BEGIN_C_DECLS

struct slist {
	size_t          sz;
	void           *data;
	struct slist   *next;
};

struct dlist {
	size_t          sz;
	void           *data;
	struct dlist   *prev;
	struct dlist   *next;
};

typedef void (*free_func) (void *);
typedef void (*list_itr_fn) (void *, size_t, void *, size_t);
typedef void (*list_print_fn) (void *);

struct slist *slist_add PARAMS((struct slist *, void *, size_t));
void slist_clear PARAMS((struct slist *));
void slist_fn_clear PARAMS((struct slist *, free_func));
void slist_fn_itr PARAMS((struct slist *, void *, size_t, list_itr_fn));
struct slist *slist_sort(struct slist *, int(*cmpfunc)(void *, void *));
void slist_print PARAMS((struct slist *, list_print_fn));
struct slist *slist_get_nth PARAMS((struct slist *, int));
struct slist *slist_search PARAMS((struct slist *, void *, size_t));
int slist_delete PARAMS((struct slist *, void *, size_t));

struct dlist *dlist_add PARAMS((struct dlist *, void *, size_t));
void dlist_delete PARAMS((struct dlist *));
struct dlist *dlist_search PARAMS((struct dlist *, void *, size_t));
void dlist_clear PARAMS((struct dlist *));
void dlist_fn_clear PARAMS((struct dlist *, free_func));
void dlist_fn_itr PARAMS((struct dlist *, void *, size_t, list_itr_fn));
struct dlist *dlist_sort(struct dlist *, int(*cmpfunc)(void *, void *));
void dlist_print PARAMS((struct dlist *, list_print_fn));

END_C_DECLS
#endif				/* _LLIST_H */
