/*-
 * Copyright (C) 2009 Micheal Smith <xulfer at me.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project author nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef HASH_H
#define HASH_H

#include <llist.h>

#undef BEGIN_C_DECLS
#undef END_C_DECLS
#ifdef __cplusplus
#define BEGIN_C_DECLS extern "C" {
#define END_C_DECLS }
#else
#define BEGIN_C_DECLS
#define END_C_DECLS
#endif

#undef PARAMS
#if defined(__STDC__) || defined(_AIX) ||                                      \
    (defined(__mips) && defined(_SYSTYPE_SVR4)) || defined(WIN32) ||           \
    defined(__cplusplus)
#define PARAMS(protos) protos
#else
#define PARAMS(protos) ()
#endif

BEGIN_C_DECLS
typedef void (*ht_free_fn)(void *, void *);
typedef list_print_fn ht_print_fn;

struct hashtable {
    size_t elements;
    size_t size;
    int seed;
    void **hashtable;
};

struct hashvalue {
    size_t ksz;
    size_t vsz;
    void *key;
    void *value;
};

enum ht_errors {
    HT_OK = 0,
    HT_CHAIN,
    HT_ARGUMENT,
    HT_DUPLICATE,
    HT_LINK,
    HT_NOTFOUND,
    HT_DELETE,
    HT_EXPAND
};

struct hashtable *ht_init PARAMS((size_t));
void ht_clear PARAMS((struct hashtable *));
int ht_insert PARAMS((struct hashtable *, void *, size_t, void *, size_t));
int ht_delete PARAMS((struct hashtable *, void *, size_t));
int ht_get PARAMS((struct hashtable *, void *, size_t, void **, size_t *));
struct hashtable *ht_expand PARAMS((struct hashtable * old_ht, size_t newsz));
void ht_fn_clear PARAMS((struct hashtable *, ht_free_fn));
void ht_print PARAMS((struct hashtable *, ht_print_fn));

END_C_DECLS
#endif /* _HASH_H */
