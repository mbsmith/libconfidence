/*
 * llist.c
 * Copyright (C) Micheal Smith 2008 <xulfer@me.com>
 *
 * main.c is free software copyrighted by Micheal Smith.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name ``Micheal Smith'' nor the name of any other
 *    contributor may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * llist.c IS PROVIDED BY Micheal Smith ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL Micheal Smith OR ANY OTHER CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <sys/types.h>

#include <errno.h>
#include <llist.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/*
 * This will keep copies of data provided a size greater
 * than zero is given.  Otherwise handling of the data
 * memory shall be left to the caller, or to the
 * *_fn_* family of functions.
 */
struct slist *slist_add(struct slist *listp, void *d, size_t sz) {
    struct slist *nextp;

    nextp = malloc(sizeof *nextp);
    nextp->next = NULL;
    nextp->sz = sz;

    if (sz == 0) {
        nextp->data = d;
    } else {
        nextp->data = malloc(sz);
        memcpy(nextp->data, d, sz);
    }

    if (listp != NULL)
        nextp->next = listp;

    return nextp;
}

/*
 * If the size of the data is 0 then this function will
 * not free the data.  In these cases slist_fn_clear should
 * be used.
 */
void slist_clear(struct slist *h) {
    struct slist *j = NULL, *k = NULL;

    j = h;

    do {
        k = j->next;
        if (j->sz > 0)
            free(j->data);
        free(j);
        j = NULL;
    } while ((j = k) != NULL);
}

void slist_fn_clear(struct slist *headp, free_func fnp) {
    struct slist *j = NULL, *k = NULL;

    j = headp;

    do {
        k = j->next;
        (*fnp)(j->data);
        free(j);
        j = NULL;
    } while ((j = k) != NULL);
}

void slist_print(struct slist *headp, list_print_fn fnp) {
    struct slist *lp = NULL;

    lp = headp;

    while (lp != NULL) {
        (*fnp)(lp->data);
        lp = lp->next;
    }
}

struct slist *slist_get_nth(struct slist *l, int n) {
    int cn = 0;
    struct slist *m = l;

    do {

        if (m == NULL)
            break;

        if (cn == n)
            return m;

        cn++;

    } while ((m = m->next) != NULL);

    return NULL;
}

void slist_fn_itr(struct slist *lp, void *data, size_t dsz, list_itr_fn itrf) {
    struct slist *nlp = lp;

    if (lp == NULL)
        return;

    while (nlp != NULL) {
        (*itrf)(nlp->data, nlp->sz, data, dsz);
        nlp = nlp->next;
    }
}

struct slist *slist_search(struct slist *haystack, void *needle, size_t nlen) {
    struct slist *itr = haystack;

    if (itr == NULL)
        return NULL;

    while (itr != NULL) {

        if (memcmp(needle, itr->data, nlen) == 0)
            return itr;

        itr = itr->next;
    }

    return NULL;
}

int slist_delete(struct slist *haystack, void *needle, size_t nlen) {
    struct slist *i, *j;

    i = haystack;

    if ((j = slist_search(haystack, needle, nlen)) == NULL) {
        return 1;
    }

    while (i->next != j)
        i = i->next;

    i->next = j->next;
    free(j->data);
    free(j);

    return 0;
}

/*
 * Mergesort implementation for singly linked lists
 */
static struct slist *split_slist(struct slist *lp) {
    struct slist *lp1, *lp2, *lp3;

    lp1 = lp2 = lp;

    if (lp == NULL)
        return NULL;

    do {
        lp3 = lp2;
        lp2 = lp2->next;
        lp1 = lp1->next;
        if (lp1 != NULL)
            lp1 = lp1->next;
    } while (lp1 != NULL);

    lp3->next = NULL;

    return lp2;
}

static struct slist *merge_slists(struct slist *lp1, struct slist *lp2,
                                  int (*cmpfunc)(void *, void *)) {
    struct slist node;
    struct slist *outlist = &node;

    node.next = outlist;

    while ((lp1 != NULL) && (lp2 != NULL)) {
        if ((*cmpfunc)(lp1->data, lp2->data) <= 0) {
            outlist->next = lp1;
            outlist = lp1;
            lp1 = lp1->next;
        } else {
            outlist->next = lp2;
            outlist = lp2;
            lp2 = lp2->next;
        }
    }

    if (lp1 != NULL)
        outlist->next = lp1;
    if (lp2 != NULL)
        outlist->next = lp2;

    if (node.next == &node)
        return NULL;

    return node.next;
}

struct slist *slist_sort(struct slist *lp, int (*cmpfunc)(void *, void *)) {
    struct slist *outlist;

    if ((lp != NULL) && (lp->next != NULL)) {
        outlist = split_slist(lp);
        lp = merge_slists(slist_sort(lp, cmpfunc), slist_sort(outlist, cmpfunc),
                          cmpfunc);
    }

    return lp;
}

struct dlist *dlist_add(struct dlist *lp, void *data, size_t dsz) {
    struct dlist *nlp;

    nlp = malloc(sizeof *nlp);

    if (dsz == 0) {
        nlp->data = data;
    } else {
        nlp->data = malloc(dsz);
        memcpy(nlp->data, data, dsz);
    }

    nlp->sz = dsz;

    if (lp == NULL) {
        nlp->next = NULL;
        nlp->prev = NULL;
    } else {
        lp->prev = nlp;
        nlp->next = lp;
        nlp->prev = NULL;
    }

    return nlp;
}

void dlist_delete(struct dlist *lp) {
    struct dlist *nlp, *plp;

    if (lp == NULL)
        return;

    plp = lp->prev;
    nlp = lp->next;

    if (plp != NULL)
        plp->next = nlp;
    if (nlp != NULL)
        nlp->prev = plp;

    if (lp->sz > 0)
        free(lp->data);
    free(lp);
}

struct dlist *dlist_search(struct dlist *haystack, void *needle, size_t nlen) {
    struct dlist *itr = haystack;

    if (itr == NULL)
        return NULL;

    while (itr != NULL) {

        if (memcmp(haystack->data, needle, nlen) == 0)
            return itr;

        itr = itr->next;
    }

    return NULL;
}

void dlist_clear(struct dlist *lp) {
    struct dlist *dlp = lp, *plp;

    if (lp == NULL)
        return;

    while (dlp->prev != NULL)
        dlp = dlp->prev;

    while (dlp != NULL) {
        plp = dlp;
        dlp = dlp->next;

        if (plp->sz > 0)
            free(plp->data);
        free(plp);
    }
}

void dlist_fn_clear(struct dlist *lp, free_func fnp) {
    struct dlist *dlp = lp, *plp;

    if (lp == NULL)
        return;

    while (dlp->prev != NULL)
        dlp = dlp->prev;

    while (dlp != NULL) {
        plp = dlp;
        dlp = dlp->next;

        (*fnp)(plp->data);
        free(plp);
    }
}

void dlist_fn_itr(struct dlist *lp, void *data, size_t dsz, list_itr_fn itrf) {
    struct dlist *dlp = lp;

    if (lp == NULL)
        return;

    while (dlp->prev != NULL)
        dlp = dlp->prev;

    while (dlp != NULL) {
        (*itrf)(dlp->data, dlp->sz, data, dsz);
        dlp = dlp->next;
    }
}

static struct dlist *split_dlist(struct dlist *lp) {
    struct dlist *lp1, *lp2, *lp3;

    lp1 = lp2 = lp;

    if (lp == NULL)
        return NULL;

    do {
        lp3 = lp2;
        lp2 = lp2->next;
        lp1 = lp1->next;
        if (lp1 != NULL)
            lp1 = lp1->next;
    } while (lp1 != NULL);

    lp3->next = NULL;
    lp3->prev = NULL;

    return lp2;
}

static struct dlist *merge_dlists(struct dlist *lp1, struct dlist *lp2,
                                  int (*cmpfunc)(void *, void *)) {
    struct dlist *outlist;
    struct dlist node;

    outlist = &node;
    node.next = outlist;
    node.prev = NULL;

    while ((lp1 != NULL) && (lp2 != NULL)) {
        if (cmpfunc(lp1->data, lp2->data) <= 0) {
            outlist->next = lp1;
            lp1->prev = outlist;
            outlist = lp1;
            lp1 = lp1->next;
        } else {
            outlist->next = lp2;
            lp2->prev = outlist;
            outlist = lp2;
            lp2 = lp2->next;
        }
    }

    if (lp1 != NULL) {
        outlist->next = lp1;
        lp1->prev = outlist;
    }
    if (lp2 != NULL) {
        outlist->next = lp2;
        lp2->prev = outlist;
    }

    if (node.next == &node)
        return NULL;

    outlist = node.next;
    outlist->prev = NULL;

    return outlist;
}

struct dlist *dlist_sort(struct dlist *lp, int (*cmpfunc)(void *, void *)) {
    struct dlist *outlist;

    if ((lp != NULL) && (lp->next != NULL)) {
        outlist = split_dlist(lp);
        lp = merge_dlists(dlist_sort(lp, cmpfunc), dlist_sort(outlist, cmpfunc),
                          cmpfunc);
    }

    return lp;
}

void dlist_print(struct dlist *lp, list_print_fn pfp) {
    struct dlist *dlp = lp;

    if (lp == NULL)
        return;

    while (dlp->prev != NULL)
        dlp = dlp->prev;

    while (dlp != NULL) {
        (*pfp)(dlp->data);
        dlp = dlp->next;
    }
}
