/*
 * Copyright (C) 2009 Micheal Smith <xulfer at me.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project author nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <config.h>

#include <conf.h>
#include <hash.h>
#include <parser.h>

#define CONF_DEFAULT_HASH_SIZE 128

extern FILE *yyin;
extern int yyparse();
extern int conf_parse(struct config *);
int conf_errno = CONF_OK;

/*
 * Error definitions
 */
static struct config_error conf_error[] = {
    {CONF_HASH, "Error inserting, or retrieving config hash value"},
    {CONF_LINE_PARSE, "An unknown line parsing error occurred"},
    {CONF_PARSE, "Error parsing configuration file."},
    {CONF_SYNTAX, "An unknown syntax error occurred"},
    {CONF_NOTFOUND, "Couldn't retrieve value from config hash"},
    {CONF_INSERT, "Error inserting value into the config hash"},
    {CONF_INIT, "There was an error while initializing the config structure"},
    {CONF_BADTYPE,
     "A different type was found when a line, or list was expected"},
    {CONF_ERRNO, ""},
    {0, ""}};

static int _dl_count;

/*
 * - From OpenBSD -
 * Copy src to string dst of size siz.  At most siz-1 characters
 * will be copied.  Always NUL terminates (unless siz == 0).
 * Returns strlen(src); if retval >= siz, truncation occurred.
 */
#ifndef HAVE_STRLCPY
static size_t strlcpy(char *dst, const char *src, size_t siz) {
    char *d = dst;
    const char *s = src;
    size_t n = siz;

    /*
     * Copy as many bytes as will fit
     */
    if (n != 0) {
        while (--n != 0) {
            if ((*d++ = *s++) == '\0')
                break;
        }
    }

    /*
     * Not enough room in dst, add NUL and traverse rest of src
     */
    if (n == 0) {
        if (siz != 0)
            *d = '\0'; /* NUL-terminate dst */
        while (*s++)
            ;
    }

    return (s - src - 1); /* count does not include NUL */
}
#endif

void conf_perror(const char *prefix) {
    int i = 0;

    if (conf_errno == CONF_OK)
        return;

    if (conf_errno == CONF_ERRNO) {
        perror(prefix);
        return;
    }

    while (conf_error[i].errstr[0] != 0) {

        if (conf_error[i].error == conf_errno) {
            printf("Error %d: %s\n", conf_error[i].error, conf_error[i].errstr);
            return;
        }

        i++;
    }
}

char *conf_strerror(int error, char *str, size_t sz) {
    int i = 0;

    if (error == CONF_OK)
        return NULL;

    if (error == CONF_ERRNO) {

        if (strerror_r(error, str, sz) != 0) {
            conf_errno = CONF_ERRNO;
            conf_perror("config_parser");
            return NULL;
        }

        return str;
    }

    while (conf_error[i].errstr[0] != 0) {

        if (conf_error[i].error == conf_errno) {
            strlcpy(str, conf_error[i].errstr, sz);
            return str;
        }

        i++;
    }

    return NULL;
}

static struct config *conf_init(const char *path) {
    struct config *conf;

    conf = malloc(sizeof *conf);
    conf->filename = NULL;

    if ((conf->conf_handle = fopen(path, "r")) == NULL) {
        conf_errno = CONF_ERRNO;
        free(conf);
        return NULL;
    }

    yyin = conf->conf_handle;

    conf->filename = strdup(path);
    conf->config_hash = ht_init(CONF_DEFAULT_HASH_SIZE);
    return conf;
}

static void conf_free_fn(void *key, void *data) {
    struct config_value *cv = (struct config_value *)data;

    if (cv == NULL)
        return;

    switch (cv->type) {
    case CONF_LINE_VALUE:
        if (cv->value.line != NULL)
            free(cv->value.line);
        break;

    case CONF_LIST_VALUE:
        if (cv->value.values != NULL)
            dlist_clear(cv->value.values);
        break;

    default:
        /*
         * Shouldn't ever happen
         */
        break;
    }

    free(key);
    free(cv->varname);
    free(cv);
}

void conf_close(struct config *conf) {
    if (conf == NULL)
        return;
    if (conf->filename != NULL)
        free(conf->filename);
    if (conf->config_hash)
        ht_fn_clear(conf->config_hash, conf_free_fn);
    free(conf);
}

static void conf_dl_print_cb(void *data) {
    char *val = (char *)data;

    if (val == NULL)
        return;

    printf("%d: %s ", _dl_count++, val);
}

static void conf_ht_print_cb(void *data) {
    static int count = 0;
    struct config_value *cv;

    if (data == NULL)
        return;

    cv = (struct config_value *)data;

    switch (cv->type) {
    case CONF_LINE_VALUE:
        printf("[%d]LINE %s: %s\n", count++, cv->varname, cv->value.line);
        break;

    case CONF_LIST_VALUE:
        printf("[%d]LIST %s: ", count++, cv->varname);
        _dl_count = 0;
        dlist_print(cv->value.values, conf_dl_print_cb);
        putchar('\n');
        break;
    }
}

struct dlist *conf_get_list(struct config *conf, char *key) {
    size_t ksz;
    struct config_value *cv;
    struct dlist *dl;

    if ((conf == NULL) || (key == NULL))
        return NULL;

    if (ht_get(conf->config_hash, key, strlen(key), (void *)&cv, &ksz) !=
        HT_OK) {
        conf_errno = CONF_NOTFOUND;
        return NULL;
    }

    if (cv->type == CONF_LINE_VALUE) {
        conf_errno = CONF_BADTYPE;
        return NULL;
    }

    if ((dl = cv->value.values) == NULL) {
        conf_errno = CONF_NOTFOUND;
        return NULL;
    }

    return dl;
}

char *conf_get_line(struct config *conf, char *key) {
    size_t ksz;
    struct config_value *cv;

    if ((conf == NULL) || (key == NULL))
        return NULL;

    if (ht_get(conf->config_hash, key, strlen(key), (void *)&cv, &ksz) !=
        HT_OK) {
        conf_errno = CONF_NOTFOUND;
        return NULL;
    }

    if (cv->type == CONF_LIST_VALUE) {
        conf_errno = CONF_BADTYPE;
        return NULL;
    }

    return cv->value.line;
}

int conf_add_line(struct config *conf, char *key, char *value) {
    struct config_value cv;
    int err;

    cv.type = CONF_LINE_VALUE;
    cv.varname = key;
    cv.value.line = value;

    err = ht_insert(conf->config_hash, key, strlen(key), &cv, sizeof cv);
    if (err == HT_EXPAND) {
        conf->config_hash =
            ht_expand(conf->config_hash, conf->config_hash->size * 2);
    } else if (err != HT_OK) {
        conf_errno = CONF_INSERT;
        return -1;
    }

    return 0;
}

int conf_add_list(struct config *conf, char *key, struct dlist *value) {
    struct config_value cv;
    int err;

    cv.type = CONF_LIST_VALUE;
    cv.varname = key;
    cv.value.values = value;

    err = ht_insert(conf->config_hash, key, strlen(key), &cv, sizeof cv);
    if (err == HT_EXPAND) {
        conf->config_hash =
            ht_expand(conf->config_hash, conf->config_hash->size * 2);
    } else if (err != HT_OK) {
        conf_errno = CONF_INSERT;
        return -1;
    }

    return 0;
}

int conf_add_env(struct config *conf, char *key, char *value) {
    char *env_val;
    char *val;

    env_val = getenv(value);
    if (env_val == NULL)
        val = strdup("");
    else
        val = strdup(env_val);

    return conf_add_line(conf, key, val);
}

struct config *parse_config(const char *path) {
    struct config *conf;

    if ((conf = conf_init(path)) == NULL) {
        conf_errno = CONF_INIT;
        conf_close(conf);
        return NULL;
    }

    if (conf_parse(conf) != 0) {
        /*
         * Parse error
         */
        conf_errno = CONF_PARSE;
        return NULL;
    }

#ifdef DEBUG
    ht_print(conf->config_hash, conf_ht_print_cb);
#endif

    return conf;
}
