/*
 * Copyright (C) 2009 Micheal Smith <xulfer at me.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the project author nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <hash.h>
#include <llist.h>

static uint32_t seed = 0;

/*
 * ----------------------------------------------------------------------------
 * MurmurHash3 was written by Austin Appleby, and is placed in the public
 * domain. The author hereby disclaims copyright to this source code.
 *
 * Note - The x86 and x64 versions do _not_ produce the same results, as the
 * algorithms are optimized for their respective platforms. You can still
 * compile and run any of them on any platform, but your performance with the
 * non-native version will be less than optimal.
 */

static inline uint32_t rotl32(uint32_t x, int8_t r) {
    return (x << r) | (x >> (32 - r));
}

static inline uint64_t rotl64(uint64_t x, int8_t r) {
    return (x << r) | (x >> (64 - r));
}

#define ROTL32(x, y) rotl32(x, y)
#define ROTL64(x, y) rotl64(x, y)

#define BIG_CONSTANT(x) (x##LLU)

/*
 *-----------------------------------------------------------------------------
 * Block read - if your platform needs to do endian-swapping or can only
 * handle aligned reads, do the conversion here
 */

static inline uint32_t getblock32(const uint32_t *p, int i) { return p[i]; }

static inline uint64_t getblock64(const uint64_t *p, int i) { return p[i]; }

/*
 *-----------------------------------------------------------------------------
 * Finalization mix - force all bits of a hash block to avalanche
 */

static inline uint32_t fmix32(uint32_t h) {
    h ^= h >> 16;
    h *= 0x85ebca6b;
    h ^= h >> 13;
    h *= 0xc2b2ae35;
    h ^= h >> 16;

    return h;
}

/*----------*/

static inline uint64_t fmix64(uint64_t k) {
    k ^= k >> 33;
    k *= BIG_CONSTANT(0xff51afd7ed558ccd);
    k ^= k >> 33;
    k *= BIG_CONSTANT(0xc4ceb9fe1a85ec53);
    k ^= k >> 33;

    return k;
}

/* ------------------------------------------------------------------------- */

void MurmurHash3_32(const void *key, int len, uint32_t seed, void *out) {
    const uint8_t *data = (const uint8_t *)key;
    const int nblocks = len / 4;

    uint32_t h1 = seed;

    const uint32_t c1 = 0xcc9e2d51;
    const uint32_t c2 = 0x1b873593;

    /*
     *----------
     * body
     */

    const uint32_t *blocks = (const uint32_t *)(data + nblocks * 4);

    for (int i = -nblocks; i; i++) {
        uint32_t k1 = getblock32(blocks, i);

        k1 *= c1;
        k1 = ROTL32(k1, 15);
        k1 *= c2;

        h1 ^= k1;
        h1 = ROTL32(h1, 13);
        h1 = h1 * 5 + 0xe6546b64;
    }

    /*
     *----------
     *  tail
     */

    const uint8_t *tail = (const uint8_t *)(data + nblocks * 4);

    uint32_t k1 = 0;

    switch (len & 3) {
    case 3:
        k1 ^= tail[2] << 16;
    case 2:
        k1 ^= tail[1] << 8;
    case 1:
        k1 ^= tail[0];
        k1 *= c1;
        k1 = ROTL32(k1, 15);
        k1 *= c2;
        h1 ^= k1;
    };

    /*
     *----------
     * finalization
     */

    h1 ^= len;

    h1 = fmix32(h1);

    *(uint32_t *)out = h1;
}

#ifdef __ARCH_32__

/*-----------------------------------------------------------------------------*/

void MurmurHash3_128(const void *key, const int len, uint32_t seed, void *out) {
    const uint8_t *data = (const uint8_t *)key;
    const int nblocks = len / 16;

    uint32_t h1 = seed;
    uint32_t h2 = seed;
    uint32_t h3 = seed;
    uint32_t h4 = seed;

    const uint32_t c1 = 0x239b961b;
    const uint32_t c2 = 0xab0e9789;
    const uint32_t c3 = 0x38b34ae5;
    const uint32_t c4 = 0xa1e38b93;

    /*
     *----------
     * body
     */

    const uint32_t *blocks = (const uint32_t *)(data + nblocks * 16);

    for (int i = -nblocks; i; i++) {
        uint32_t k1 = getblock32(blocks, i * 4 + 0);
        uint32_t k2 = getblock32(blocks, i * 4 + 1);
        uint32_t k3 = getblock32(blocks, i * 4 + 2);
        uint32_t k4 = getblock32(blocks, i * 4 + 3);

        k1 *= c1;
        k1 = ROTL32(k1, 15);
        k1 *= c2;
        h1 ^= k1;

        h1 = ROTL32(h1, 19);
        h1 += h2;
        h1 = h1 * 5 + 0x561ccd1b;

        k2 *= c2;
        k2 = ROTL32(k2, 16);
        k2 *= c3;
        h2 ^= k2;

        h2 = ROTL32(h2, 17);
        h2 += h3;
        h2 = h2 * 5 + 0x0bcaa747;

        k3 *= c3;
        k3 = ROTL32(k3, 17);
        k3 *= c4;
        h3 ^= k3;

        h3 = ROTL32(h3, 15);
        h3 += h4;
        h3 = h3 * 5 + 0x96cd1c35;

        k4 *= c4;
        k4 = ROTL32(k4, 18);
        k4 *= c1;
        h4 ^= k4;

        h4 = ROTL32(h4, 13);
        h4 += h1;
        h4 = h4 * 5 + 0x32ac3b17;
    }

    /*
     *----------
     * tail
     */

    const uint8_t *tail = (const uint8_t *)(data + nblocks * 16);

    uint32_t k1 = 0;
    uint32_t k2 = 0;
    uint32_t k3 = 0;
    uint32_t k4 = 0;

    switch (len & 15) {
    case 15:
        k4 ^= tail[14] << 16;
    case 14:
        k4 ^= tail[13] << 8;
    case 13:
        k4 ^= tail[12] << 0;
        k4 *= c4;
        k4 = ROTL32(k4, 18);
        k4 *= c1;
        h4 ^= k4;

    case 12:
        k3 ^= tail[11] << 24;
    case 11:
        k3 ^= tail[10] << 16;
    case 10:
        k3 ^= tail[9] << 8;
    case 9:
        k3 ^= tail[8] << 0;
        k3 *= c3;
        k3 = ROTL32(k3, 17);
        k3 *= c4;
        h3 ^= k3;

    case 8:
        k2 ^= tail[7] << 24;
    case 7:
        k2 ^= tail[6] << 16;
    case 6:
        k2 ^= tail[5] << 8;
    case 5:
        k2 ^= tail[4] << 0;
        k2 *= c2;
        k2 = ROTL32(k2, 16);
        k2 *= c3;
        h2 ^= k2;

    case 4:
        k1 ^= tail[3] << 24;
    case 3:
        k1 ^= tail[2] << 16;
    case 2:
        k1 ^= tail[1] << 8;
    case 1:
        k1 ^= tail[0] << 0;
        k1 *= c1;
        k1 = ROTL32(k1, 15);
        k1 *= c2;
        h1 ^= k1;
    };

    /*
     *----------
     * finalization
     */

    h1 ^= len;
    h2 ^= len;
    h3 ^= len;
    h4 ^= len;

    h1 += h2;
    h1 += h3;
    h1 += h4;
    h2 += h1;
    h3 += h1;
    h4 += h1;

    h1 = fmix32(h1);
    h2 = fmix32(h2);
    h3 = fmix32(h3);
    h4 = fmix32(h4);

    h1 += h2;
    h1 += h3;
    h1 += h4;
    h2 += h1;
    h3 += h1;
    h4 += h1;

    ((uint32_t *)out)[0] = h1;
    ((uint32_t *)out)[1] = h2;
    ((uint32_t *)out)[2] = h3;
    ((uint32_t *)out)[3] = h4;
}

#elif __ARCH_64__

/*-----------------------------------------------------------------------------*/

void MurmurHash3_128(const void *key, const int len, const uint32_t seed,
                     void *out) {
    const uint8_t *data = (const uint8_t *)key;
    const int nblocks = len / 16;

    uint64_t h1 = seed;
    uint64_t h2 = seed;

    const uint64_t c1 = BIG_CONSTANT(0x87c37b91114253d5);
    const uint64_t c2 = BIG_CONSTANT(0x4cf5ad432745937f);

    /*
     *----------
     * body
     */

    const uint64_t *blocks = (const uint64_t *)(data);

    for (int i = 0; i < nblocks; i++) {
        uint64_t k1 = getblock64(blocks, i * 2 + 0);
        uint64_t k2 = getblock64(blocks, i * 2 + 1);

        k1 *= c1;
        k1 = ROTL64(k1, 31);
        k1 *= c2;
        h1 ^= k1;

        h1 = ROTL64(h1, 27);
        h1 += h2;
        h1 = h1 * 5 + 0x52dce729;

        k2 *= c2;
        k2 = ROTL64(k2, 33);
        k2 *= c1;
        h2 ^= k2;

        h2 = ROTL64(h2, 31);
        h2 += h1;
        h2 = h2 * 5 + 0x38495ab5;
    }

    /*
     *----------
     * tail
     */

    const uint8_t *tail = (const uint8_t *)(data + nblocks * 16);

    uint64_t k1 = 0;
    uint64_t k2 = 0;

    switch (len & 15) {
    case 15:
        k2 ^= ((uint64_t)tail[14]) << 48;
    case 14:
        k2 ^= ((uint64_t)tail[13]) << 40;
    case 13:
        k2 ^= ((uint64_t)tail[12]) << 32;
    case 12:
        k2 ^= ((uint64_t)tail[11]) << 24;
    case 11:
        k2 ^= ((uint64_t)tail[10]) << 16;
    case 10:
        k2 ^= ((uint64_t)tail[9]) << 8;
    case 9:
        k2 ^= ((uint64_t)tail[8]) << 0;
        k2 *= c2;
        k2 = ROTL64(k2, 33);
        k2 *= c1;
        h2 ^= k2;

    case 8:
        k1 ^= ((uint64_t)tail[7]) << 56;
    case 7:
        k1 ^= ((uint64_t)tail[6]) << 48;
    case 6:
        k1 ^= ((uint64_t)tail[5]) << 40;
    case 5:
        k1 ^= ((uint64_t)tail[4]) << 32;
    case 4:
        k1 ^= ((uint64_t)tail[3]) << 24;
    case 3:
        k1 ^= ((uint64_t)tail[2]) << 16;
    case 2:
        k1 ^= ((uint64_t)tail[1]) << 8;
    case 1:
        k1 ^= ((uint64_t)tail[0]) << 0;
        k1 *= c1;
        k1 = ROTL64(k1, 31);
        k1 *= c2;
        h1 ^= k1;
    };

    /*
     *----------
     * finalization
     */

    h1 ^= len;
    h2 ^= len;

    h1 += h2;
    h2 += h1;

    h1 = fmix64(h1);
    h2 = fmix64(h2);

    h1 += h2;
    h2 += h1;

    ((uint64_t *)out)[0] = h1;
    ((uint64_t *)out)[1] = h2;
}

#endif

/*
 * End of MurmurHash3 implementation.
 *-----------------------------------------------------------------------------
 */

static ht_print_fn ht_print_fnp;
static ht_free_fn ht_free_fnp;

struct hashtable *ht_init(size_t sz) {
    unsigned int i;
    struct hashtable *h;
    h = malloc(sizeof *h);
    h->hashtable = malloc(sz * sizeof *h->hashtable);

    for (i = 0; i < sz; ++i) {
        h->hashtable[i] = NULL;
    }

    h->size = sz;
    h->seed = rand();
    h->elements = 0;

    return h;
}

void hash_free_func(void *data) {
    struct hashvalue *hv;

    hv = (struct hashvalue *)data;
    free(hv->key);
    free(hv->value);
    free(hv);
}

void ht_clear(struct hashtable *ht) {
    unsigned int i;
    struct slist *link;

    for (i = 0; i < ht->size; ++i) {

        if ((link = ht->hashtable[i]) != NULL) {
            slist_fn_clear(link, hash_free_func);
        }
    }

    free(ht->hashtable);
    free(ht);
}

static void ht_free_cb(void *data) {
    struct hashvalue *hv;

    if (data == NULL)
        return;

    hv = (struct hashvalue *)data;
    (*ht_free_fnp)(hv->key, hv->value);
    free(hv);
}

void ht_fn_clear(struct hashtable *ht, ht_free_fn ffn) {
    unsigned int i;
    struct slist *link;

    ht_free_fnp = ffn;

    for (i = 0; i < ht->size; ++i) {

        if ((link = ht->hashtable[i]) != NULL) {
            slist_fn_clear(link, ht_free_cb);
        }
    }

    free(ht->hashtable);
    free(ht);
}

static void ht_print_cb(void *data) {
    struct hashvalue *hv;

    if (data == NULL)
        return;

    hv = (struct hashvalue *)data;
    (*ht_print_fnp)(hv->value);
}

void ht_print(struct hashtable *ht, ht_print_fn hpf) {
    unsigned int i;
    struct slist *link;

    ht_print_fnp = hpf;

    for (i = 0; i < ht->size; ++i) {

        if ((link = ht->hashtable[i]) != NULL) {
            slist_print(link, ht_print_cb);
        }
    }
}

struct hashtable *ht_expand(struct hashtable *old_ht, size_t newsz) {
    unsigned int i;
    struct hashtable *ht;
    struct hashvalue *hv;
    struct slist *link, *plink;

    if (newsz <= old_ht->size)
        return NULL;

    ht = ht_init(newsz);

    for (i = 0; i < old_ht->size; ++i) {
        link = old_ht->hashtable[i];

        while (link != NULL) {
            hv = (struct hashvalue *)link->data;

            if (ht_insert(ht, hv->key, hv->ksz, hv->value, hv->vsz) != HT_OK) {
                return NULL;
            }

            free(hv->key);
            free(hv->value);
            free(link->data);
            plink = link;
            free(plink);
            link = link->next;
        }
    }

    return ht;
}

struct slist *ht_search(struct hashtable *ht, uint32_t *hk, void *k,
                        size_t ksz) {
    struct slist *link;
    struct hashvalue *hv;
    uint32_t hkey;

    MurmurHash3_32(k, ksz, ht->seed, &hkey);
    *hk = hkey % ht->size;

    link = ht->hashtable[*hk];

    if (link == NULL)
        return NULL;

    while (link != NULL) {
        hv = (struct hashvalue *)link->data;

        if (memcmp(k, hv->key, ksz < hv->ksz ? ksz : hv->ksz) == 0) {
            return link;
        }

        link = link->next;
    }

    return NULL;
}

int ht_insert(struct hashtable *ht, void *k, size_t ksz, void *v, size_t vsz) {
    struct slist *link = NULL;
    struct hashvalue *hv;
    uint32_t hk = 0;

    if ((k == NULL) || (ksz == 0))
        return HT_ARGUMENT;

    if (ht_search(ht, &hk, k, ksz) != NULL) {
        return HT_DUPLICATE;
    }

    link = ht->hashtable[hk];
    hv = malloc(sizeof *hv);
    hv->key = malloc(ksz);
    memcpy(hv->key, k, ksz);
    hv->ksz = ksz;
    hv->value = malloc(vsz);
    memcpy(hv->value, v, vsz);
    hv->vsz = vsz;

    if ((link = slist_add(link, (void *)hv, 0)) == NULL) {
        free(hv->key);
        free(hv->value);
        free(hv);
        return HT_LINK;
    }

    ht->hashtable[hk] = link;
    ht->elements++;

    if (ht->elements >= ht->size)
        return HT_EXPAND;

    return HT_OK;
}

int ht_delete(struct hashtable *ht, void *k, size_t ksz) {
    struct slist *link;
    uint32_t hk;

    MurmurHash3_32(k, ksz, ht->seed, &hk);
    hk %= ht->size;

    link = ht->hashtable[hk];

    if (link == NULL)
        return HT_NOTFOUND;

    if (slist_delete(link, k, ksz))
        return HT_DELETE;

    ht->elements--;
    return HT_OK;
}

int ht_get(struct hashtable *ht, void *k, size_t ksz, void **v, size_t *vsz) {
    struct slist *link = NULL;
    struct hashvalue *hv;
    uint32_t hk = 0;

    if ((k == NULL) || (ksz == 0))
        return HT_ARGUMENT;

    if ((link = ht_search(ht, &hk, k, ksz)) == NULL)
        return HT_NOTFOUND;

    hv = (struct hashvalue *)link->data;
    *v = hv->value;
    *vsz = hv->vsz;

    return HT_OK;
}
