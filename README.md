## Libconfidence

### Synopsis

This is a lightweight, and efficient config parser library.  This is a great alternative to say XML, or JSON.
Of course I speak only for configuration file purposes.  Configuration files often don't need the features provided
by XML, and JSON.  INI files certainly fit this role as well.  However that often isn't enough.  This is my
attempt at a decent compromise.  The C like syntax affords a bit more flexibility than INI, but doesn't contain
all the bloat that JSON/XML does.

The library is also very compatible.  Currently it compiles, and runs on essentially any POSIX compliant platform.
If anyone finds a case where this isn't true then please let me know and I'll attempt to rectify it.

### Compilation, and Installation

This is a standard CMake build setup.  Compilation is straight forward.

E.g.

```sh
$ mkdir build; cd build
$ cmake ../
$ make
$ make install
```

### Using Libconfidence In Your Project

This of course depends on your build setup.  However a very simple example follows.

```sh
$ clang -o example -lconfidence example.c
```

### File Format

As mentioned above the file format is quite C like.  The entire format isn't going to be described here.

However here is a short sample:

#### Sample
```c
/*
 * This is my configuration file.
 */

a_variable = "a_value";
```

### Library Usage

Similar to the file format section I'll also defer to both the wiki located at https://github.com/mbsmith/libconfidence/wiki/Usage,
and the Doxygen documentation located at http://mbsmith.bitbucket.org/libconfidence.  However here is a quick example.

#### Usage Example
```c
struct config *conf;
char *my_var;

conf = parse_config("/home/user/example.conf");
my_var = conf_get_line(conf, "example_var");
printf("example_var = %s\n", my_var);
close_conf(conf);
```
